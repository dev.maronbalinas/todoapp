export interface User {
  id: number;
  username: string;
  password: string;
}

export interface Todo {
  id: number;
  title: string;
  priority: number;
  is_completed: boolean;
  comments?: Array<[]>;
}

export type RootStackParamList = {
  TodoList: undefined;
  TodoView: { todo: Todo };
};

export interface Comment {
  id: number;
  content: string;
  user: any;
  created_at: string;
}
