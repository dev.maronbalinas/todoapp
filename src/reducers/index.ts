import { combineReducers } from '@reduxjs/toolkit';
import authReducer from './auth/authSlice';
import todosReducer from './todos/todosSlices';

const rootReducer = combineReducers({
  auth: authReducer,
  todos: todosReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
