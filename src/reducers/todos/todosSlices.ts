import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Todo, Comment } from '../types';

interface TodoState {
  todos: Todo[];
}

const initialState: TodoState = {
  todos: [],
};

const saveTodosToLocalStorage = async newTodo => {
  try {
    const existingTodos = await loadTodosFromLocalStorage();
    const updatedTodos = [...existingTodos, newTodo];
    await AsyncStorage.setItem('todos', JSON.stringify(updatedTodos));
  } catch (error) {
    console.error('Error saving todos to storage:', error);
  }
};

const loadTodosFromLocalStorage = async () => {
  try {
    const todosJSON = await AsyncStorage.getItem('todos');
    if (todosJSON) {
      return JSON.parse(todosJSON);
    }
    return [];
  } catch (error) {
    console.error('Error loading todos from AsyncStorage:', error);
    return [];
  }
};

const todoSlice = createSlice({
  name: 'todos',
  initialState,
  reducers: {
    setTodos: (state, action: PayloadAction<Todo[]>) => {
      state.todos = action.payload;
    },
    addTodo: (state, action: PayloadAction<Todo>) => {
      const newTodoId = state.todos.length + 1;
      const newTodo: Todo = { ...action.payload, id: newTodoId, comments: [] };
      state.todos.push(newTodo);
      saveTodosToLocalStorage(newTodo);
    },
    updateTodo: (
      state,
      action: PayloadAction<{ id: number; title: string; priority: number }>,
    ) => {
      const { id, title, priority } = action.payload;
      const todoIndex = state.todos.findIndex(todo => todo.id === id);
      if (todoIndex !== -1) {
        state.todos[todoIndex] = {
          ...state.todos[todoIndex],
          title: title,
          priority: priority,
        };
        AsyncStorage.setItem('todos', JSON.stringify(state.todos));
      } else {
        console.error('Task not found');
      }
    },
    deleteTodo: (state, action: PayloadAction<number>) => {
      state.todos = state.todos.filter(
        todo => todo.id !== Number(action.payload),
      );
      saveTodosToLocalStorage(state.todos);
    },
    completeTodo: (state, action: PayloadAction<number>) => {
      const todoIndex = state.todos.findIndex(
        todo => todo.id === action.payload,
      );
      if (todoIndex !== -1) {
        state.todos[todoIndex] = {
          ...state.todos[todoIndex],
          is_completed: true,
        };
        AsyncStorage.setItem('todos', JSON.stringify(state.todos));
      } else {
        console.error('Task not found');
      }
    },
    addCommentToTodo(
      state,
      action: PayloadAction<{
        id: number;
        comment: Comment;
        user_id: number;
        username: string;
      }>,
    ) {
      const { id, comment, user_id, username } = action.payload;

      const todoIndex = state.todos.findIndex(todo => todo.id === id);

      if (todoIndex !== -1) {
        const updatedTodo = { ...state.todos[todoIndex] };

        if (!Array.isArray(updatedTodo.comments)) {
          updatedTodo.comments = [];
        }

        const commentId = updatedTodo.comments.length + 1;

        updatedTodo.comments.push({
          id: commentId,
          todo_id: id,
          comment: comment,
          user: {
            user_id,
            username,
          },
        });

        const updatedTodos = [
          ...state.todos.slice(0, todoIndex),
          updatedTodo,
          ...state.todos.slice(todoIndex + 1),
        ];

        state.todos = updatedTodos;

        AsyncStorage.setItem('todos', JSON.stringify(updatedTodos));
      } else {
        console.error('Task not found');
      }
    },
  },
});

export const {
  setTodos,
  addTodo,
  updateTodo,
  deleteTodo,
  completeTodo,
  addCommentToTodo,
} = todoSlice.actions;

export default todoSlice.reducer;
