import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { User } from '../types';

interface AuthState {
  users: User[];
  currentUser: User | null;
}

const initialState: AuthState = {
  users: [],
  currentUser: null,
};

const saveUsersToLocalStorage = async (newUser: User) => {
  try {
    const existingUsers = await loadUsersFromLocalStorage();
    const updatedUsers = [...existingUsers, newUser];
    await AsyncStorage.setItem('users', JSON.stringify(updatedUsers));
  } catch (error) {
    console.error('Error saving users to storage:', error);
  }
};

const loadUsersFromLocalStorage = async () => {
  try {
    const usersJSON = await AsyncStorage.getItem('users');
    if (usersJSON) {
      return JSON.parse(usersJSON);
    }
    return [];
  } catch (error) {
    console.error('Error loading users from AsyncStorage:', error);
    return [];
  }
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    loginSuccess: (state, action: PayloadAction<User>) => {
      state.currentUser = action.payload;
    },
    logoutSuccess(state) {
      state.currentUser = null;
      AsyncStorage.removeItem('user');
    },
    registerSuccess(
      state,
      action: PayloadAction<{ username: string; password: string }>,
    ) {
      const { username, password } = action.payload;
      const newUserId = state.users.length + 1;
      const newUser: User = { id: newUserId, username, password };
      state.currentUser = newUser;
      saveUsersToLocalStorage(state.currentUser);
    },
  },
});

export const { loginSuccess, logoutSuccess, registerSuccess } =
  authSlice.actions;

export default authSlice.reducer;
