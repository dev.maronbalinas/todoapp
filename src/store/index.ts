import { configureStore } from '@reduxjs/toolkit';
import rootReducer from '../reducers';
import { fetchTodosFromLocalStorage } from './todoMiddleware';

const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat(fetchTodosFromLocalStorage),
});

export default store;
