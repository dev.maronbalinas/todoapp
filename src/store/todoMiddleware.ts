import { Middleware } from 'redux';
import { RootState } from '../reducers';
import { setTodos } from '../reducers/todos/todosSlices';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const fetchTodosFromLocalStorage: Middleware<{}, RootState> =
  store => next => async (action: any) => {
    if (action?.type === 'todos/init') {
      try {
        const todos = await AsyncStorage.getItem('todos');
        console.log('todosFetch', todos);
        if (todos !== null) {
          store.dispatch(setTodos(JSON.parse(todos)));
        }
      } catch (error) {
        console.error('Error fetching todos from local storage:', error);
      }
    }
    return next(action);
  };
