import React, { ReactNode } from 'react';
import { StyleSheet } from 'react-native';
import { Header as HeaderRNE } from '@rneui/themed';
import { SafeAreaProvider } from 'react-native-safe-area-context';

interface HeaderProps {
  title: string;
  leftComponent?: ReactNode | null | undefined;
  rightComponent?: ReactNode | null | undefined;
}

const Header: React.FC<HeaderProps> = ({
  title,
  leftComponent,
  rightComponent,
}) => {
  return (
    <HeaderRNE
      leftComponent={leftComponent}
      rightComponent={rightComponent}
      centerComponent={{ text: title, style: styles.heading }}
    />
  );
};

const styles = StyleSheet.create({
  heading: {
    color: 'white',
    fontSize: 22,
    fontWeight: 'bold',
  },
  headerRight: {
    flexDirection: 'row',
    marginTop: 5,
  },
});

export default Header;
