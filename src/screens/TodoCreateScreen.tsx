import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { useDispatch } from 'react-redux';
import { useNavigation, ParamListBase } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { addTodo } from '../reducers/todos/todosSlices';
import { Input, Icon } from '@rneui/themed';
import { Header, Button } from '../components';

import { useForm, Controller } from 'react-hook-form';

import { ButtonGroup } from '@rneui/themed';

const TodoCreateScreen = () => {
  const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>();
  const dispatch = useDispatch();

  const { control, handleSubmit, reset } = useForm();

  const onSubmit = (data: any) => {
    const todoData = { ...data, is_completed: false };
    dispatch(addTodo(todoData));
    navigation.goBack();
    reset();
  };

  const handleOnBackNavigation = () => {
    navigation.goBack();
  };

  const priorityColor = (value: Number) => {
    let color: string;
    switch (value) {
      case 1:
        color = 'skyblue';
        break;
      case 2:
        color = 'yellow';
        break;
      case 3:
        color = 'red';
        break;
      default:
        color = 'lightgray';
        break;
    }
    return color;
  };

  return (
    <View style={styles.container}>
      <Header
        title="Add Task"
        leftComponent={
          <Button onPress={handleOnBackNavigation}>
            <Icon name="arrow-back" color="white" />
          </Button>
        }
      />

      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <Input
            placeholder="Title"
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
          />
        )}
        name="title"
        defaultValue=""
      />

      <Controller
        control={control}
        render={({ field: { onChange, value } }) => (
          <>
            <ButtonGroup
              buttons={['Low', 'Normal', 'High', 'Urgent']}
              selectedIndex={Number(value)}
              onPress={onChange}
              containerStyle={{ marginBottom: 20 }}
              selectedButtonStyle={{ backgroundColor: priorityColor(value) }}
              selectedTextStyle={{ color: 'black', fontWeight: 'bold' }}
            />
          </>
        )}
        name="priority"
        defaultValue=""
      />
      <Button onPress={handleSubmit(onSubmit)}>
        <Text>Submit</Text>
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    width: '80%',
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
});

export default TodoCreateScreen;
