import React from 'react';
import { View, Button, StyleSheet } from 'react-native';
import { useDispatch } from 'react-redux';
import { useNavigation, ParamListBase } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { Input } from '@rneui/themed';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { loginSuccess } from '../reducers/auth/authSlice';
import * as yup from 'yup';

const validationSchema = yup.object().shape({
  username: yup.string().required('Username is required'),
  password: yup.string().required('Password is required'),
});

const LoginScreen = () => {
  const dispatch = useDispatch();
  const {
    control,
    handleSubmit,
    setError,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>();

  const handleRegisterNavigation = () => {
    navigation.navigate('Register');
    reset();
  };

  const onSubmit = async data => {
    const users = await AsyncStorage.getItem('users');

    if (users) {
      const userList = JSON.parse(String(users));
      const user = userList.find(user => user.username === data.username);

      if (!user) {
        setError('username', {
          type: 'manual',
          message: 'User not found!',
        });
        return;
      }

      if (user.password !== data.password) {
        setError('password', {
          type: 'manual',
          message: 'Incorrect password!',
        });
        return;
      }

      dispatch(loginSuccess(user));
      navigation.navigate('Main');
      reset();
    }
    setError('username', {
      type: 'manual',
      message: 'User not found!',
    });
  };

  return (
    <View style={styles.container}>
      <Controller
        control={control}
        render={({ field: { onChange, value } }) => (
          <Input
            placeholder="Username"
            onChangeText={onChange}
            value={value}
            errorMessage={errors.username?.message}
          />
        )}
        name="username"
        defaultValue=""
      />
      <Controller
        control={control}
        render={({ field: { onChange, value } }) => (
          <Input
            placeholder="Password"
            secureTextEntry
            onChangeText={onChange}
            value={value}
            errorMessage={errors.password?.message}
          />
        )}
        name="password"
        defaultValue=""
      />
      <Button title="Login" onPress={handleSubmit(onSubmit)} />
      <Button title="Register" onPress={handleRegisterNavigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    width: '80%',
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
});

export default LoginScreen;
