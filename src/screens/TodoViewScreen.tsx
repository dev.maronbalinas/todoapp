import React, { useEffect, useState, useCallback } from 'react';
import { View, StyleSheet, Text, FlatList } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import {
  useNavigation,
  ParamListBase,
  useFocusEffect,
} from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

import { updateTodo, addCommentToTodo } from '../reducers/todos/todosSlices';
import { Input, Icon, ListItem, Button as ButtonRNE } from '@rneui/themed';
import { Header, Button } from '../components';

import { useForm, Controller } from 'react-hook-form';

import { RootState } from '../reducers';

import { ButtonGroup } from '@rneui/themed';

const TodoViewScreen = ({ route }) => {
  const { todoId } = route.params;

  const [todo, setTodo] = useState(null);
  const todos = useSelector((state: RootState) => state.todos.todos);

  useFocusEffect(
    useCallback(() => {
      const todoItem = todos.find(item => item.id === todoId);
      if (todoItem) {
        setTodo(todoItem);
      } else {
        setTodo(null); // Set the state to null if todoItem is undefined
      }
    }, [todos, todoId]),
  );

  const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>();
  const auth = useSelector((state: RootState) => state.auth.currentUser);

  const dispatch = useDispatch();

  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    reset(todo);
  }, [todo]);

  const onSubmitEdit = (data: any) => {
    const { id, title, priority } = data;
    dispatch(updateTodo({ id, title, priority }));
    reset({ ...todo, priority, title });
  };

  const [commentForm, enableCommentForm] = useState(false);
  const toggleAddComment = () => enableCommentForm(!commentForm);

  const onSubmitComment = (data: any) => {
    dispatch(
      addCommentToTodo({
        ...data,
        user_id: auth?.id,
        username: auth?.username,
      }),
    );
    reset();
    toggleAddComment();
  };

  const handleOnBackNavigation = () => {
    navigation.goBack();
  };

  const priorityColor = (value: Number) => {
    let color: string;
    switch (value) {
      case 1:
        color = 'skyblue';
        break;
      case 2:
        color = 'yellow';
        break;
      case 3:
        color = 'red';
        break;
      default:
        color = 'lightgray';
        break;
    }
    return color;
  };

  const renderItem = ({ item }) => (
    <ListItem.Swipeable
      bottomDivider
      leftWidth={80}
      rightWidth={90}
      rightContent={() => (
        <ButtonRNE
          containerStyle={{
            flex: 1,
            justifyContent: 'center',
            backgroundColor: 'red',
          }}
          type="clear"
          icon={{ name: 'delete-outline', color: 'white' }}
          // onPress={() => handleDeleteTodo(item.id)}
        />
      )}>
      <ListItem.Content>
        <ListItem.Subtitle style={{ color: 'gray' }}>
          {item.user?.username}
        </ListItem.Subtitle>
        <ListItem.Title>{item.comment}</ListItem.Title>
      </ListItem.Content>
      <ListItem.Chevron />
    </ListItem.Swipeable>
  );

  return (
    <View style={styles.container}>
      <Header
        title={todo?.title}
        leftComponent={
          <Button onPress={handleOnBackNavigation}>
            <Icon name="arrow-back" color="white" />
          </Button>
        }
        rightComponent={
          <Button onPress={handleSubmit(onSubmitEdit)}>
            <Icon name="save" color="white" />
          </Button>
        }
      />
      <View>
        <Controller
          control={control}
          render={({ field: { onChange, onBlur, value } }) => (
            <Input
              placeholder="Title"
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
            />
          )}
          name="title"
          defaultValue=""
        />

        <Controller
          control={control}
          render={({ field: { onChange, value } }) => (
            <>
              <ButtonGroup
                buttons={['Low', 'Normal', 'High', 'Urgent']}
                selectedIndex={Number(value)}
                onPress={onChange}
                containerStyle={{ marginBottom: 20 }}
                selectedButtonStyle={{ backgroundColor: priorityColor(value) }}
                selectedTextStyle={{ color: 'black', fontWeight: 'bold' }}
              />
            </>
          )}
          name="priority"
          defaultValue=""
        />
      </View>
      <FlatList
        data={todo?.comments}
        extraData={[todo?.comments]}
        contentContainerStyle={{ flex: 1 }}
        renderItem={renderItem}
        ListEmptyComponent={
          <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>No Comment(s)</Text>
          </View>
        }
        keyExtractor={item => item.id}
      />

      <View style={{ padding: 16 }}>
        {commentForm ? (
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Controller
              control={control}
              render={({ field: { onChange, value } }) => (
                <Input
                  placeholder="Write Comment"
                  onChangeText={onChange}
                  value={value}
                  containerStyle={{ width: '85%' }}
                  errorMessage={errors.password?.message}
                />
              )}
              name="comment"
              rules={{ required: true }}
              defaultValue=""
            />
            <ButtonRNE
              onPress={handleSubmit(onSubmitComment)}
              containerStyle={{
                justifyContent: 'center',
              }}
              type="clear"
              icon={{ name: 'send' }}
            />
          </View>
        ) : (
          <ButtonRNE onPress={toggleAddComment}>
            <Text style={{ color: 'white' }}>Add Comment</Text>
          </ButtonRNE>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    width: '80%',
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
});

export default TodoViewScreen;
