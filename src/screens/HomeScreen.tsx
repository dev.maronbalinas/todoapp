import React, { useState, useEffect } from 'react';
import { View, FlatList } from 'react-native';
import {
  Icon,
  Button as ButtonRNE,
  BottomSheet,
  ListItem,
} from '@rneui/themed';
import { Header, Button } from '../components';

import { useNavigation, ParamListBase } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../reducers';
import { deleteTodo, completeTodo } from '../reducers/todos/todosSlices';
import { Todo } from '../reducers/types';

import { orderBy } from 'lodash';

const HomeScreen = () => {
  const dispatch = useDispatch();
  const todos = useSelector((state: RootState) => state.todos.todos);
  useEffect(() => {
    dispatch({ type: 'todos/init' });
  }, [dispatch]);

  const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>();

  const handleTodoCreateNavigation = () => {
    navigation.navigate('TodoCreate');
  };

  const handleTodoViewNavigation = (item: any) => () => {
    navigation.navigate('TodoView', { todoId: item.id });
  };

  const handleDeleteTodo = (todoId: number) => {
    dispatch(deleteTodo(todoId));
  };

  const handleCompletedTodo = (todoId: number) => {
    dispatch(completeTodo(todoId));
  };

  const priorityColor = (value: Number) => {
    let color: string;
    switch (value) {
      case 1:
        color = 'skyblue';
        break;
      case 2:
        color = 'yellow';
        break;
      case 3:
        color = 'red';
        break;
      default:
        color = 'lightgray';
        break;
    }
    return color;
  };

  const [isVisible, setIsVisible] = useState(false);
  const [orderByPriority, setOrderByPriority] = useState(true);

  const toggleBottomSheet = () => {
    setIsVisible(!isVisible);
  };

  const toggleOrderCriteria = (orderByPriority: boolean) => {
    setOrderByPriority(orderByPriority);
    toggleBottomSheet();
  };

  const orderTodos = (todos: Todo[]) => {
    if (orderByPriority) {
      return orderBy(todos, ['priority'], ['desc']);
    } else {
      return orderBy(todos, ['title'], ['asc']);
    }
  };

  const orderedTodos = orderTodos(todos);

  return (
    <View style={{ flex: 1 }}>
      <Header
        title="Tasks"
        leftComponent={
          <>
            <Button onPress={() => setIsVisible(true)}>
              <Icon name="sort" color="white" />
            </Button>
            <BottomSheet modalProps={{}} isVisible={isVisible}>
              <ListItem onPress={() => toggleOrderCriteria(true)}>
                <ListItem.Content>
                  <ListItem.Title>Sort by Priority</ListItem.Title>
                </ListItem.Content>
              </ListItem>
              <ListItem onPress={() => toggleOrderCriteria(false)}>
                <ListItem.Content>
                  <ListItem.Title>Sort by Name</ListItem.Title>
                </ListItem.Content>
              </ListItem>
              <ListItem onPress={toggleBottomSheet}>
                <ListItem.Content>
                  <ListItem.Title>Cancel</ListItem.Title>
                </ListItem.Content>
              </ListItem>
            </BottomSheet>
          </>
        }
        rightComponent={
          <Button onPress={handleTodoCreateNavigation}>
            <Icon name="add" color="white" />
          </Button>
        }
      />

      <FlatList
        data={orderedTodos}
        renderItem={({ item }) => (
          <ListItem.Swipeable
            onPress={handleTodoViewNavigation(item)}
            leftWidth={80}
            rightWidth={90}
            leftContent={action => (
              <ButtonRNE
                containerStyle={{
                  flex: 1,
                  justifyContent: 'center',
                  backgroundColor: '#f4f4f4',
                }}
                type="clear"
                icon={{
                  name: 'archive-outline',
                  type: 'material-community',
                }}
                onPress={action}
              />
            )}
            rightContent={() => (
              <ButtonRNE
                containerStyle={{
                  flex: 1,
                  justifyContent: 'center',
                  backgroundColor: 'red',
                }}
                type="clear"
                icon={{ name: 'delete-outline', color: 'white' }}
                onPress={() => handleDeleteTodo(item.id)}
              />
            )}>
            <Icon
              name="flag"
              type="ionicons"
              onPress={() => setIsVisible(true)}
              color={priorityColor(item.priority)}
            />
            <ListItem.Content>
              <ListItem.Title>{item.title}</ListItem.Title>
            </ListItem.Content>
            <ButtonRNE
              type="clear"
              icon={{
                name: item.is_completed ? 'check-circle' : 'circle-thin',
                color: 'green',
                type: 'font-awesome',
              }}
              onPress={() => handleCompletedTodo(item.id)}
            />

            <ListItem.Chevron />
          </ListItem.Swipeable>
        )}
        keyExtractor={item => item.id}
      />
    </View>
  );
};

export default HomeScreen;
