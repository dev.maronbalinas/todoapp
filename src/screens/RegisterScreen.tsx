import React from 'react';
import { View, Button, StyleSheet } from 'react-native';
import { useDispatch } from 'react-redux';
import { registerSuccess } from '../reducers/auth/authSlice';
import { useNavigation, ParamListBase } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { Input } from '@rneui/themed';

import AsyncStorage from '@react-native-async-storage/async-storage';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import * as yup from 'yup';

const validationSchema = yup.object().shape({
  username: yup.string().required('Username is required'),
  password: yup.string().required('Password is required'),
});

const RegisterScreen = () => {
  const dispatch = useDispatch();
  const {
    control,
    handleSubmit,
    setError,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>();

  const onSubmit = async data => {
    const users = await AsyncStorage.getItem('users');
    const userList = JSON.parse(String(users));

    if (users) {
      const existingUser = userList.find(
        user => user.username === data.username,
      );
      if (existingUser) {
        setError('username', {
          type: 'manual',
          message: 'Username already exists!',
        });
        return;
      }
    }
    dispatch(registerSuccess(data));
    navigation.navigate('Main');
    reset();
  };

  const handleGoBack = () => {
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <Controller
        control={control}
        render={({ field: { onChange, value } }) => (
          <Input
            placeholder="Set Username"
            onChangeText={onChange}
            value={value}
            errorMessage={errors.username?.message}
          />
        )}
        name="username"
        defaultValue=""
      />
      <Controller
        control={control}
        render={({ field: { onChange, value } }) => (
          <Input
            placeholder="Set Password"
            secureTextEntry
            onChangeText={onChange}
            value={value}
            errorMessage={errors.password?.message}
          />
        )}
        name="password"
        defaultValue=""
      />
      <Button title="Register" onPress={handleSubmit(onSubmit)} />
      <Button title="Back to Login" onPress={handleGoBack} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    width: '80%',
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
});

export default RegisterScreen;
