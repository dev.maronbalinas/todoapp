import React, { useEffect } from 'react';
import { View, Text } from 'react-native';
import { Header, Button } from '../components';

import {
  useNavigation,
  ParamListBase,
  CommonActions,
} from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../reducers';
import { logoutSuccess } from '../reducers/auth/authSlice';
const ProfileScreen = () => {
  const dispatch = useDispatch();
  const todos = useSelector((state: RootState) => state.todos.todos);
  const auth = useSelector((state: RootState) => state.auth.currentUser);
  const completedTasksCount = todos.filter(todos => todos.is_completed).length;
  const totalTasksCount = todos.length;

  useEffect(() => {
    dispatch({ type: 'todos/init' });
  }, [dispatch]);

  const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>();

  const handleLogout = () => {
    dispatch(logoutSuccess());
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [
          {
            name: 'Auth',
          },
        ],
      }),
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <Header
        title="Profile"
        rightComponent={
          <Button onPress={handleLogout}>
            <Text>Logout</Text>
          </Button>
        }
      />
      <View style={{ flex: 1, margin: 16 }}>
        <Text>Welcome {auth?.username}!</Text>
        <View
          style={{
            flex: 0.3,
            margin: 16,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{ fontWeight: 'bold', fontSize: 50, marginBottom: 16 }}>
            {completedTasksCount}/{totalTasksCount}
          </Text>
          <Text>Task Completed</Text>
        </View>
      </View>
    </View>
  );
};

export default ProfileScreen;
