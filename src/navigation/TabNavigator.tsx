import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { HomeScreen, ProfileScreen } from '../screens';

import { Icon } from '@rneui/themed';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={({ route }) => ({
        headerShown: false,
        tabBarIcon: ({ color }) => {
          let iconName;
          if (route.name === 'Task') {
            iconName = 'tasks';
          } else {
            iconName = 'user';
          }

          return (
            <Icon
              name={iconName}
              size={20}
              color={color}
              type="font-awesome-5"
            />
          );
        },
        tabBarInactiveTintColor: 'gray',
        style: {
          backgroundColor: 'white',
        },
      })}>
      <Tab.Screen name="Task" component={HomeScreen} />
      <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
  );
};

export default TabNavigator;
