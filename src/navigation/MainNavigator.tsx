import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import TabNavigator from './TabNavigator';
import { TodoCreateScreen, TodoViewScreen } from '../screens';
const Stack = createNativeStackNavigator();

const MainNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="Tab"
      screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Tab" component={TabNavigator} />
      <Stack.Screen name="TodoCreate" component={TodoCreateScreen} />
      <Stack.Screen name="TodoView" component={TodoViewScreen} />
    </Stack.Navigator>
  );
};

export default MainNavigator;
